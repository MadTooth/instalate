/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.os.Parcel;
import android.os.Parcelable;

public class Language implements Parcelable {
    private final Integer mIdx;
    private final String mName;
    private int mIcon;

    public Language(Integer idx, String name) {
        mIdx = idx;
        mName = name;
    }

    public Integer getIdx() {
        return mIdx;
    }
    public String getName() {
        return mName;
    }
    public int getIcon() {
        return mIcon;
    }
    public void setIcon(int icon) {
        mIcon = icon;
    }


    protected Language(Parcel in) {
        if (in.readByte() == 0) {
            mIdx = null;
        } else {
            mIdx = in.readInt();
        }
        mName = in.readString();
        mIcon = in.readInt();
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (mIdx == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(mIdx);
        }
        dest.writeString(mName);
        dest.writeInt(mIcon);
    }
    @Override
    public int describeContents() {
        return 0;
    }
    public static final Creator<Language> CREATOR = new Creator<Language>() {
        @Override
        public Language createFromParcel(Parcel in) {
            return new Language(in);
        }

        @Override
        public Language[] newArray(int size) {
            return new Language[size];
        }
    };
}
