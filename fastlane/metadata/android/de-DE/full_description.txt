Funktionen:
- übersichtliche Anzeige der Übersetzung in einem halbtransparenten Fenster
- viele verschiedene Übersetzungsdienste
- 3 verschiedene <i>Triggermethoden</i>:
  1. Kontextmenü
  2. "Teilen" Funktion
  3. Button (selektierter Text muss zunächst in die Zwischenablage kopiert werden)
      - frei verschiebbar via Drag-and-Drop
      - nur Android Versionen < 10: automatische Anzeige des Buttons

Berechtigungen:
- <b>INTERNET</b>:
  Erlaubt InstaLate sich mit dem Internet zu verbinden, um Übersetzungen nachzuschlagen
- <b>ACCESS_NETWORK_STATE</b>:
  Erlaubt InstaLate zu überprüfen, ob das Gerät mit dem Internet verbunden ist
- <b>FOREGROUND_SERVICE</b>:
  Erlaubt InstaLate einen Service zu starten, der Änderungen der Zwischenablage erkennt und einen Button anzeigt
- <b>RECEIVE_BOOT_COMPLETED</b>:
  Erlaubt InstaLate diesen Service wahlweise nach dem Hochfahren des Geräts zu starten
- <b>SYSTEM_ALERT_WINDOW</b>:
  Erlaubt InstaLate den Button über anderen Apps anzuzeigen

Privatsphäre:
- Keine Speicherung des Suchverlaufs seitens dieser App, weder lokal noch remote (lediglich die letzte Suche wird im Arbeitsspeicher zwischengespeichert). Allerdings kann es sein, dass Suchanfragen von dem jeweils ausgewählten Übersetzungsdienst protokolliert werden.
- Kein Client-seitiges JavaScript wird ausgeführt
- HTTPS Kommunikation zwischen App und Übersetzungsdiensten

<b>Bitte seien Sie besonders vorsichtig und geben Sie nicht versehentlich sensible Daten (Passwörter etc.) an Übersetzungsdienste weiter! Wenn Sie den Button verwenden, lösen Sie keine Übersetzung aus, wenn sich sensible Daten in der Zwischenablage befinden.</b>

Tipps und Tricks:
- Wenn eine App ein benutzerdefiniertes Kontextmenü implementiert hat, prüfen Sie, ob der Button für Sie funktioniert oder ob die App Text teilen kann
- Integration in "Librera" (FOSS E-Book Reader):
   1. Selektieren Sie ein beliebiges Wort im Dokument (Textselektion muss vom Dokument unterstützt werden)
   2. Tippen Sie im sich öffnenden Dialog auf das "+" Zeichen am unteren Rand des Dialogs und wählen Sie InstaLate
   3. Wenn Sie das nächste Mal Text auswählen, haben Sie Zugriff auf den InstaLate Button am unteren Rand des Dialogs

Danke:
An alle Übersetzungsanbieter und ihre großartigen Dienste, ohne die diese Anwendung nicht viel wert wäre (in alphabetischer Reihenfolge):
- Beolingus: https://dict.tu-chemnitz.de/
- DeepL: https://www.deepl.com/
- Dict. cc: https://www.dict.cc/
- GNU CIDE: https://gcide.gnu.org.ua/
- Heinzelnisse: https://www.heinzelnisse.info/
- LibreTranslate: https://translate.argosopentech.com/
- Linguee: https://www.linguee.com/
- WikDict: https://www.wikdict.com/
- Wiktionary: https://wiktionary.org/
