/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.Context;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.concept1tech.unn.ArrayUtils;
import com.concept1tech.unn.DroidUtils;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import java.util.ArrayList;
import java.util.List;

public class TranslationData implements Parcelable {

    private PageRequest mRequest = new PageRequest();
    private PageResponse mResponse = new PageResponse();
    private Integer[] mActiveLangs;
    private String mProviderId;
    private Uri mBrowserUrl;
    private String mOrigText;
    private String mCleanedText;
    private List<ParcelableCharSequence> mTList = new ArrayList<>();
    private final Language mSrcLang;
    private final Language mTgtLang;

    public TranslationData(Context ctx, String text) {
        mOrigText = text;
        mActiveLangs = ArrayUtils.parseInt(App.getActiveLangsFromSPrefs(ctx));
        String[] activeLangNames = ctx.getResources().getStringArray(R.array.dicts_languages);

        Integer srcIdx = mActiveLangs[mActiveLangs[1] == 2 ? 2 : 0];    // take account of backwards direction
        Integer tgtIdx = mActiveLangs[mActiveLangs[1] == 2 ? 0 : 2];
        mSrcLang = new Language(srcIdx, activeLangNames[srcIdx]);
        mTgtLang = new Language(tgtIdx, activeLangNames[tgtIdx]);

        mProviderId = Provider.getCurrentId(ctx);
        Provider curProvider = Provider.getById(ctx, mProviderId);
        mCleanedText = curProvider.cleanText(mOrigText);
        if (!mCleanedText.isEmpty()) {
            curProvider.preProcess(ctx, this);
        }
    }

    public PageRequest getRequest() {
        return mRequest;
    }
    public void setRequest(PageRequest request) {
        mRequest = request;
    }
    public PageResponse getResponse() {
        return mResponse;
    }
    public void setResponse(PageResponse response) {
        mResponse = response;
    }

    public Integer[] getActiveLangs() {
        return mActiveLangs;
    }
    public void setActiveLangs(Integer[] activeLangs) {
        mActiveLangs = activeLangs;
    }
    public String getProviderId() {
        return mProviderId;
    }
    public void setProviderId(String id) {
        mProviderId = id;
    }
    /**
     * @return URI to be viewed in the browser (defaults to search request URI if not set by {@link #setBrowserUrl(Uri)})
     */
    public Uri getBrowserUrl() {
        return mBrowserUrl != null ? mBrowserUrl : mRequest.getUri();
    }
    /**
     * Set this to view this URI in the browser instead of the search request URI.
     *
     * @param browserUrl URI to be viewed in the browser
     */
    public void setBrowserUrl(Uri browserUrl) {
        mBrowserUrl = browserUrl;
    }
    public String getOrigText() {
        return mOrigText;
    }
    public void setOrigText(String origText) {
        mOrigText = origText;
    }
    public String getCleanedText() {
        return mCleanedText;
    }
    public void setCleanedText(String cleanedText) {
        mCleanedText = cleanedText;
    }
    public List<ParcelableCharSequence> getTranslationList() {
        return mTList;
    }
    public void setTranslationList(List<ParcelableCharSequence> tList) {
        mTList = tList;
    }
    public Language getSrcLang() {
        return mSrcLang;
    }
    public Language getTgtLang() {
        return mTgtLang;
    }


    public void postProcess(Context ctx) {
        Provider.getById(ctx, mProviderId).postProcess(ctx, this);
    }

    public boolean equalsRequest(TranslationData data) {
        if (data != null) {
            return this.getRequest().equals(data.getRequest());
        }
        return false;
    }


    protected TranslationData(Parcel in) {
        mRequest = in.readParcelable(PageRequest.class.getClassLoader());
        mResponse = in.readParcelable(PageResponse.class.getClassLoader());
        mActiveLangs = DroidUtils.createIntegerArray(in);
        mProviderId = in.readString();
        mBrowserUrl = in.readParcelable(Uri.class.getClassLoader());
        mOrigText = in.readString();
        mCleanedText = in.readString();
        mTList = DroidUtils.readListFromParcel(in, mTList, ParcelableCharSequence.class);
        mSrcLang = in.readParcelable(Language.class.getClassLoader());
        mTgtLang = in.readParcelable(Language.class.getClassLoader());

    }
    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeParcelable(mRequest, flags);
        parcel.writeParcelable(mResponse, flags);
        DroidUtils.writeIntegerArray(parcel, mActiveLangs);
        parcel.writeString(mProviderId);
        parcel.writeParcelable(mBrowserUrl, flags);
        parcel.writeString(mOrigText);
        parcel.writeString(mCleanedText);
        DroidUtils.writeArrayListToParcel(parcel, mTList);
        parcel.writeParcelable(mSrcLang, flags);
        parcel.writeParcelable(mTgtLang, flags);
    }
    public static final Creator<TranslationData> CREATOR = new Creator<TranslationData>() {
        @Override
        public TranslationData createFromParcel(Parcel in) {
            return new TranslationData(in);
        }
        @Override
        public TranslationData[] newArray(int size) {
            return new TranslationData[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }


}
