/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate.Providers;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.style.LeadingMarginSpan;
import android.text.style.StyleSpan;
import android.util.Log;

import com.concept1tech.instalate.Provider;
import com.concept1tech.instalate.R;
import com.concept1tech.instalate.TranslationData;
import com.concept1tech.instalate.TranslationTask;
import com.concept1tech.unn.ArrayUtils;
import com.concept1tech.unn.HTTPHeaders;
import com.concept1tech.unn.MySpannableStringBuilder;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Arrays;
import java.util.List;

public class WikDict extends Provider {

    public static final String TAG = WikDict.class.getSimpleName();

    private final String[] mBaseUrls = {
                "https://www.wikdict.com/%2$s-%3$s/%1$s",         // bi-directional
                null,                                             // forward directional
                null};                                            // backwards directional

    // order is important
    private final String[] mLangSpecs = {
                /*[0] ENGLISH*/          "en",
                /*[1] FRENCH*/           "fr",
                /*[2] CZECH*/            null,
                /*[3] POLISH*/           "pl",
                /*[4] SLOVAK*/           null,
                /*[5] GERMAN*/           "de",
                /*[6] HUNGARIAN*/        null,
                /*[7] DUTCH*/            "nl",
                /*[8] ALBANIAN*/         null,
                /*[9] RUSSIAN*/          "ru",
                /*[10] SPANISH*/         "es",
                /*[11] SWEDISH*/         "sv",
                /*[12] ICELANDIC*/       null,
                /*[13] NORWEGIAN*/       "no",
                /*[14] ITALIAN*/         "it",
                /*[15] FINNISH*/         "fi",
                /*[16] DANISH*/          null,
                /*[17] PORTUGUESE*/      "pt",
                /*[18] CROATIAN*/        null,
                /*[19] BULGARIAN*/       "bg",
                /*[20] ROMANIAN*/        null,
                /*[21] LATIN*/           "la",
                /*[22] ESPERANTO*/       null,
                /*[23] BOSNIAN*/         null,
                /*[24] TURKISH*/         "tr",
                /*[25] SERBIAN*/         null,
                /*[26] GREEK*/           "el",
                /*[27] CHINESE*/         null,
                /*[28] JAPANESE*/        "ja",
                /*[29] SLOVENE*/         null,
                /*[30] LITHUANIAN*/      "lt",
                /*[31] LATVIAN*/         null,
                /*[32] ESTONIAN*/        null,
                /*[33] MALTESE*/         null,
                /*[34] MALAGASY*/        "mg",
                /*[35] INDONESIAN*/      "id"
    };

    public WikDict(String label, String id, int icon) {
        super(label, id, icon);

        Boolean[][][] combinations = new Boolean[3][][];
        combinations[0] = buildBiDirMat();
        combinations[1] = buildForwardsMat();
        combinations[2] = buildBackwardsMat();
        setCombinations(combinations);

//        isTest();
    }

    public void preProcess(Context c, TranslationData data) {
        PageRequest request = data.getRequest();
        String s = data.getCleanedText();
        setPageRequestDefaults(request);
        request.setHTTPHeaders(new HTTPHeaders("User-Agent", "InstaLate/7 (Android)"));      // identify ourselves as requested by WikDict webmaster
        request.setUri(parseUri(mBaseUrls, s, mLangSpecs, data.getActiveLangs()));
    }
    @Override
    public void postProcess(Context c, TranslationData data) {
        int leadingMargin1 = c.getResources().getDimensionPixelOffset(R.dimen.sp_6);
        int leadingMargin2 = c.getResources().getDimensionPixelOffset(R.dimen.sp_46);
        List<ParcelableCharSequence> tList = data.getTranslationList();
        tList.clear();
        PageResponse response = data.getResponse();
        Document doc = Jsoup.parse(response.get());

        // (1) get "div" tag with attribute "id" that has value "results"
        // (2) get "div" tag with attribute "class" that has value "lang-results" and is descendent of (1)
        Elements directions = doc.select("div#results div[class=lang-result]");
        for (Element direction : directions) {
            // (3) get "table" tag with attribute "class" that is direct child of (2)
            Elements termMatches = direction.select("> table[class]");
            // (4) get any element that has attribute "class" that has a value starting with "idioms" and
            // is a direct child of (2)
            Element idioms = direction.selectFirst("> [class^=idioms]");

            for (Element termMatch : termMatches) {

                // table header (term, class + gender, inflections + pronounciation)
                Element header = termMatch.selectFirst("> thead");
                if (header != null) {
                    MySpannableStringBuilder builder = new MySpannableStringBuilder();

                    // get all elements that have at least 1 char of text
                    Elements texElems = header.getElementsMatchingOwnText(".");
                    if (texElems.size() > 0) {
                        builder.append(texElems.get(0).ownText());  // first element should contain our term
                        for (int i = 1; i < texElems.size(); i++) {
                            builder.append(i == 1 ?  "  (" : ", ");
                            String curTex = texElems.get(i).ownText();
                            builder.append(curTex.replaceAll(" , ", ", "));
                            builder.append( i == texElems.size() - 1 ? ")" : "");
                        }
                    }

                    builder.setSpan(new StyleSpan(Typeface.BOLD),
                                0,
                                builder.length(),
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tList.add(new ParcelableCharSequence(builder));
                }


                // table body (translations + glossary)
                // glossary:         left (italic)
                // translated terms: right
                Elements glossNtranslations = termMatch.select("> tbody > tr");
                for (Element glossNtrans : glossNtranslations) {
                    // there should always be exactly 2 table-data cells per table row
                    Elements cells = glossNtrans.select("td");
                    if (cells.size() < 2) {
                        Log.w(TAG, "WikDict: Row has less than 2 elements. Skipping...");
                        continue;
                    } else if (cells.size() > 2)
                        Log.w(TAG, "WikDict: Row has more than 2 elements");

                    Element glossCell = cells.get(0);
                    Element translCell = cells.get(1);
                    MySpannableStringBuilder builder = new MySpannableStringBuilder();


                    // glossary
                    // get all list items that are direct children of unordered list
                    Elements glossItems = glossCell.select("ul > li");
                    int j = 0;
                    for (Element glossItem : glossItems) {
                        builder.append(glossItem.text());
                        builder.append(++j == glossItems.size() -1 ? ",\n": "\n");  // last item does not get a separator
                    }
                    builder.setSpan(new StyleSpan(Typeface.ITALIC),
                                0,
                                builder.length(),
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    builder.setSpan(new LeadingMarginSpan.Standard(leadingMargin1),
                                0,
                                builder.length(),
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


                    // translation
                    builder.append(translCell.text(),
                                new LeadingMarginSpan.Standard(leadingMargin2),
                                SpannedString.SPAN_EXCLUSIVE_EXCLUSIVE);

                    tList.add(new ParcelableCharSequence(builder));
                }
                tList.add(new ParcelableCharSequence(""));
            }


            // idioms
            if (idioms != null) {
                String lastTagName = "";
                // get any element that is a direct child of dl
                Elements rows = idioms.select("dl > *");
                for (Element row : rows) {
                    MySpannableStringBuilder builder = new MySpannableStringBuilder();
                    String tagName = row.tagName().toLowerCase();
                    if (tagName.equals("dt")) {
                        if (lastTagName.equals("dd")) {  // add a break after row based on changing tag names because we assume there could be multiple dt's or dd's per row
                            builder.append("\n");
                        }
                        builder.append(row.text(),
                                    new StyleSpan(Typeface.BOLD),
                                    SpannedString.SPAN_EXCLUSIVE_EXCLUSIVE);
                    } else if (tagName.equals("dd")) {
                        builder.append(row.text(),
                                    new LeadingMarginSpan.Standard(leadingMargin2),
                                    SpannedString.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    tList.add(new ParcelableCharSequence(builder));
                    lastTagName = tagName;
                }
            }


            tList.add(new ParcelableCharSequence(""));
        }


        if (tList.size() == 0) {
            response.setError(TranslationTask.ERR_PARSING_RESPONSE);
        }
        response.set("");
    }

    private Boolean[][] buildBiDirMat() {
        Boolean[][] mat = new Boolean[mLangSpecs.length][mLangSpecs.length];
        ArrayUtils.fill(mat, false);

        Boolean[] en = ArrayUtils.toBool(mLangSpecs);
        ArrayUtils.setFromArray(mat, en, 0, 0, 0);           // en
        ArrayUtils.setFromArray(mat, en.clone(), 0, 5, 0);   // de
        ArrayUtils.setFromArray(mat, en.clone(), 0, 7, 0);   // nl
        ArrayUtils.setFromArray(mat, en.clone(), 0, 11, 0);  // sv
        ArrayUtils.setFromArray(mat, en.clone(), 0, 14, 0);  // it
        ArrayUtils.setFromArray(mat, en.clone(), 0, 15, 0);  // fi
        ArrayUtils.setFromArray(mat, en.clone(), 0, 17, 0);  // pt

        Boolean[] pl = en.clone();
        pl[34] = false;
        ArrayUtils.setFromArray(mat, pl, 0, 3, 0);           // pl
        ArrayUtils.setFromArray(mat, pl.clone(), 0, 1, 0);   // fr

        Boolean[] ru = en.clone();
        ru[9] = ru[10] = ru[13] = ru[19] = ru[21] = ru[24] = ru[26] = ru[30] = ru[35] = false;
        ArrayUtils.setFromArray(mat, ru, 0, 9, 0);           // ru
        ArrayUtils.setFromArray(mat, ru.clone(), 0, 10, 0);  // es

        Boolean[] ja = en.clone();
        ja[13] = ja[21] = ja[24] = ja[26] = ja[28] = ja[35] = false;
        ArrayUtils.setFromArray(mat, ja, 0, 28, 0);          // ja

        Boolean[] bg = new Boolean[mLangSpecs.length];
        Arrays.fill(bg, false);
        bg[0] = bg[1] = bg[3] = bg[5] = bg[7] = bg[11] = bg[14] = bg[15] = bg[17] = bg[28] = true;
        ArrayUtils.setFromArray(mat, bg, 0, 19, 0);          // bg
        ArrayUtils.setFromArray(mat, bg.clone(), 0, 30, 0);  // lt

        Boolean[] mg = new Boolean[mLangSpecs.length];
        Arrays.fill(mg, false);
        mg[0] = mg[5] = mg[7] = mg[9] = mg[10] = mg[11] = mg[14] = mg[15] = mg[17] = mg[28] = true;
        ArrayUtils.setFromArray(mat, mg, 0, 34, 0);          // mg

        Boolean[] no = new Boolean[mLangSpecs.length];
        Arrays.fill(no, false);
        no[0] = no[1] = no[3] = no[5] = no[7] = no[11] = no[14] = no[15] = no[17] = true;
        ArrayUtils.setFromArray(mat, no, 0, 13, 0);          // no
        ArrayUtils.setFromArray(mat, no.clone(), 0, 21, 0);  // la
        ArrayUtils.setFromArray(mat, no.clone(), 0, 24, 0);  // tr
        ArrayUtils.setFromArray(mat, no.clone(), 0, 26, 0);  // el
        ArrayUtils.setFromArray(mat, no.clone(), 0, 35, 0);  // id

        ArrayUtils.setMainDiagonal(mat, false);
        return mat;
    }

    private Boolean[][] buildForwardsMat() {
        Boolean[][] mat = new Boolean[mLangSpecs.length][mLangSpecs.length];
        ArrayUtils.fill(mat, false);
        return mat;
    }

    private Boolean[][] buildBackwardsMat() {
        return buildForwardsMat();
    }

    private void isTest() {
        ArrayUtils.logMatrices(getCombinations(), mLangSpecs, mLangSpecs, 4);
    }
}

