# InstaLate
> Online dictionary - Translate content directly in your apps


[<img alt="Get it on F-Droid" height="80" src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png">](https://f-droid.org/en/packages/com.concept1tech.instalate/)
[<img alt="Direct APK Download" height="80" src="https://tachibanagenerallaboratories.github.io/images/badges/Direct%20Download/direct-apk-download.png">](https://gitlab.com/concept1tech/instalate/-/releases)


# Table of Contents
1. [Features](#features)
2. [Permissions](#permissions)
3. [Privacy](#privacy)
4. [System](#system)
5. [Screenshots](#screenshots)
6. [Tips and Tricks](#tips-and-tricks)
7. [Thanks](#thanks)
8. [FAQ](#faq)


## Features
- Distraction-free translation displayed in a semitransparent overlay window
- Choose from many different translation providers
- 3 different *trigger modes*:
  1. Context menu
  2. "Share" intent
  3. Floating button (requires the selection to be copied/cut to the system clipboard first)
      - Button freely movable via drag 'n' drop
      - Android versions < 10 only: show/hide button automatically


## Permissions
- **INTERNET**: Allows InstaLate to connect to the internet to lookup translations
- **ACCESS_NETWORK_STATE**: Allows InstaLate to verify if the device is connected to the internet
- **FOREGROUND_SERVICE**: Allows InstaLate to run a service that listens to clipboard changes and to show a floating button
- **RECEIVE_BOOT_COMPLETED**: Allows InstaLate to optionally start this service during start-up of the device
- **SYSTEM_ALERT_WINDOW**: Allows InstaLate to display a floating button over other apps


## Privacy
- No search history is stored locally or remotely by this app (last search term is cached in memory only) but it may or may not be possible that your search is tracked by the selected translation provider
- No client-side JavaScript is executed
- HTTPS communication between app and translation providers

*Please be extra cautious to not share sensitive data (passwords etc.) with translation providers by accident! If you're using the floating button don't trigger a translation if sensitive data is located in the system clipboard.*


## System
- Minimum Version: Android 6.0, "Marshmallow" (API 23)
- Target Version: Android 12 (API 31)


## Screenshots
<a href="fastlane/metadata/android/en-US/images/phoneScreenshots/screencast_a.gif">
  <img src="fastlane/metadata/android/en-US/images/phoneScreenshots/screencast_a.gif" alt="screencast" width="300"/>
</a>

| | | | | |
|-|-|-|-|-|
|[![](fastlane/metadata/android/en-US/images/phoneScreenshots/1.png)](fastlane/metadata/android/en-US/images/phoneScreenshots/1.png) | [![](fastlane/metadata/android/en-US/images/phoneScreenshots/2.png)](fastlane/metadata/android/en-US/images/phoneScreenshots/2.png) | [![](fastlane/metadata/android/en-US/images/phoneScreenshots/3.png)](fastlane/metadata/android/en-US/images/phoneScreenshots/3.png) | [![](fastlane/metadata/android/en-US/images/phoneScreenshots/4.png)](fastlane/metadata/android/en-US/images/phoneScreenshots/4.png) | [![](fastlane/metadata/android/en-US/images/phoneScreenshots/5.png)](fastlane/metadata/android/en-US/images/phoneScreenshots/5.png)|


## Tips and Tricks
- If an app has implemented a custom context menu see if the floating button works for you or if the app is able to share text
- Integration into "Librera" (FOSS ebook reader):
   1. 1. Select any word in the document by long-pressing it (text selection must be supported by the document)
   2. In the opening dialog tap the "+" sign at the bottom and select InstaLate
   3. Next time you select text you'll have access to the InstaLate button at the bottom


## Thanks
To all translation providers and their awesome services this application would be nothing without (in alphabetical order):
- Beolingus: [https://dict.tu-chemnitz.de/](https://dict.tu-chemnitz.de/)
- DeepL: [https://www.deepl.com/](https://www.deepl.com/)
- Dict.[]()cc: [https://www.dict.cc/](https://www.dict.cc/)
- GNU CIDE: [https://gcide.gnu.org.ua/](https://gcide.gnu.org.ua/)
- Heinzelnisse: [https://www.heinzelnisse.info/](https://www.heinzelnisse.info/)
- LibreTranslate: [https://translate.argosopentech.com/](https://translate.argosopentech.com/)
- Linguee: [https://www.linguee.com/](https://www.linguee.com/)
- WikDict: [https://www.wikdict.com/](https://www.wikdict.com/)
- Wiktionary: [https://wiktionary.org/](https://wiktionary.org/)


## FAQ
1. **Are there any plans for an offline dictionary?**
   - No. Not in the near future.
2. **Can Google Translations be included to the list of providers?**
   - No. For privacy and ethical reasons Google Translations will not be included to the list of providers
