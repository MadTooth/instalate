/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;

import androidx.preference.PreferenceManager;

import com.concept1tech.instalate.Providers.Beolingus;
import com.concept1tech.instalate.Providers.DeepL;
import com.concept1tech.instalate.Providers.Dictcc;
import com.concept1tech.instalate.Providers.Gcide;
import com.concept1tech.instalate.Providers.Heinzelnisse;
import com.concept1tech.instalate.Providers.LibreTranslate;
import com.concept1tech.instalate.Providers.Linguee;
import com.concept1tech.instalate.Providers.WikDict;
import com.concept1tech.instalate.Providers.WiktionaryLinks;
import com.concept1tech.unn.DroidUtils;
import com.concept1tech.unn.HTTPHeaders;
import com.concept1tech.unn.PageRequest;

import java.util.HashMap;
import java.util.Locale;

public abstract class Provider {

    private static HashMap<String, Provider> sProviders;

    public static HashMap<String, Provider> getAll(Context ctx) {
        if (sProviders == null) {
            sProviders = new HashMap<>();
            Resources res = ctx.getResources();
            String[] proNames = res.getStringArray(R.array.pro_names);
            String[] proValues = res.getStringArray(R.array.pro_names_values);
            int[] proIcons = DroidUtils.getResourcesArray(ctx, R.array.pro_favicons);
            sProviders.put(proValues[0], new WikDict(proNames[0], proValues[0], proIcons[0]));
            sProviders.put(proValues[1], new Dictcc(proNames[1], proValues[1], proIcons[1]));
            sProviders.put(proValues[2], new Linguee(proNames[2], proValues[2], proIcons[2]));
            sProviders.put(proValues[3], new Beolingus(proNames[3], proValues[3], proIcons[3]));
            sProviders.put(proValues[4], new WiktionaryLinks(proNames[4], proValues[4], proIcons[4]));
            sProviders.put(proValues[5], new Gcide(proNames[5], proValues[5], proIcons[5]));
            sProviders.put(proValues[6], new Heinzelnisse(proNames[6], proValues[6], proIcons[6]));
            sProviders.put(proValues[7], new DeepL(proValues[7], proValues[7], proIcons[7]));
            sProviders.put(proValues[8], new LibreTranslate(proNames[8], proValues[8], proIcons[8]));
        }
        return sProviders;
    }

    public static Provider getCurrent(Context ctx) {
        return getAll(ctx).get(getCurrentId(ctx));
    }

    public static String getCurrentId(Context ctx) {
        SharedPreferences defaultSPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        return defaultSPref.getString("prefkey_provider", "dictcc");
    }

    public static Provider getById(Context ctx, String id) {
        return getAll(ctx).get(id);
    }

    private final String mLabel;
    private final String mId;
    private final int mIcon;
    private Boolean[][][] mCombinations;    // 1st dimension: 1st element: matrix for bi-directional translation. 2nd element: matrix for forward-directional translation. 3nd element: matrix for backwards-directional translation
    private boolean isBrowserOnly = false;

    protected Provider(String label, String id, int icon) {
        mLabel = label;
        mId = id;
        mIcon = icon;
    }

    public abstract void preProcess(Context c, TranslationData data);

    public abstract void postProcess(Context c, TranslationData data);

    public String getLabel() {
        return mLabel;
    }
    public String getId() {
        return mId;
    }
    public int getIcon() {
        return mIcon;
    }
    public Boolean[][][] getCombinations() {
        return mCombinations;
    }
    public void setCombinations(Boolean[][][] combinations) {
        mCombinations = combinations;
    }
    public boolean isBrowserOnly() {
        return isBrowserOnly;
    }
    public void setBrowserOnly(boolean browserOnly) {
        isBrowserOnly = browserOnly;
    }

    public String cleanText(String s) {
        s = s.replaceAll("[^\\p{L}\\s]+", " ");  // replace all chars that are not letters (from any language) or whitespace (see: https://regex101.com/r/lP8pG9/156) with space
        return s.trim();
    }

    protected Uri parseUri(String[] bases, String s, String[] langs, Integer[] props) {
        s = Uri.encode(s);
        return Uri.parse(String.format(Locale.US, bases[props[1]], s, langs[props[0]], langs[props[2]]));
    }

    protected void setPageRequestDefaults(PageRequest request) {
        request.setCharset("UTF-8");
        request.setHTTPHeaders(HTTPHeaders.getPreset(HTTPHeaders.FIREFOX_CUR_WIN_US));
        request.setMethod(PageRequest.GET);
    }
}
