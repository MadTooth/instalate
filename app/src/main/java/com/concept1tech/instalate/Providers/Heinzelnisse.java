/*
 InstaLate (instant translation app)
 Copyright (C) 2021 Concept1Tech

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.

 */
package com.concept1tech.instalate.Providers;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spanned;
import android.text.style.LeadingMarginSpan;
import android.text.style.StyleSpan;

import com.concept1tech.instalate.Provider;
import com.concept1tech.instalate.R;
import com.concept1tech.instalate.TranslationData;
import com.concept1tech.instalate.TranslationTask;
import com.concept1tech.unn.ArrayUtils;
import com.concept1tech.unn.MySpannableStringBuilder;
import com.concept1tech.unn.PageRequest;
import com.concept1tech.unn.PageResponse;
import com.concept1tech.unn.ParcelableCharSequence;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class Heinzelnisse extends Provider {

    private final String[] mBaseUrls = {
                "https://heinzelnisse.info/searchResults?searchItem=%1$s&type=json&setOptions=true&dict%2$s%3$sSearch=on&dict%3$s%2$sSearch=on&dictExactSearch=&dictPhoneticSearch=&wikiSearch=&forumKeywordSearch=&dictNynorskSearch=&dictBokmaalSearch=",
                "https://heinzelnisse.info/searchResults?searchItem=%1$s&type=json&setOptions=true&dict%2$s%3$sSearch=on&dictExactSearch=&dictPhoneticSearch=&wikiSearch=&forumKeywordSearch=&dictNynorskSearch=&dictBokmaalSearch=",
                "https://heinzelnisse.info/searchResults?searchItem=%1$s&type=json&setOptions=true&dict%3$s%2$sSearch=on&dictExactSearch=&dictPhoneticSearch=&wikiSearch=&forumKeywordSearch=&dictNynorskSearch=&dictBokmaalSearch=",
    };
    private final String[] mBrowserUrls = {
                "https://www.heinzelnisse.info/dict?searchItem=%1$s&setOptions=true&dict%2$s%3$sSearch=on&dict%3$s%2$sSearch=on&dictExactSearch=&dictPhoneticSearch=&wikiSearch=&forumKeywordSearch=&dictNynorskSearch=&dictBokmaalSearch=",
                "https://www.heinzelnisse.info/dict?searchItem=%1$s&setOptions=true&dict%2$s%3$sSearch=on&dictExactSearch=&dictPhoneticSearch=&wikiSearch=&forumKeywordSearch=&dictNynorskSearch=&dictBokmaalSearch=",
                "https://www.heinzelnisse.info/dict?searchItem=%1$s&setOptions=true&dict%3$s%2$sSearch=on&dictExactSearch=&dictPhoneticSearch=&wikiSearch=&forumKeywordSearch=&dictNynorskSearch=&dictBokmaalSearch=",
    };

    private final String[] mLangSpecs = {
                /*[0] ENGLISH*/          null,
                /*[1] FRENCH*/           null,
                /*[2] CZECH*/            null,
                /*[3] POLISH*/           null,
                /*[4] SLOVAK*/           null,
                /*[5] GERMAN*/           "De",
                /*[6] HUNGARIAN*/        null,
                /*[7] DUTCH*/            null,
                /*[8] ALBANIAN*/         null,
                /*[9] RUSSIAN*/          null,
                /*[10] SPANISH*/         null,
                /*[11] SWEDISH*/         null,
                /*[12] ICELANDIC*/       null,
                /*[13] NORWEGIAN*/       "No",
                /*[14] ITALIAN*/         null,
                /*[15] FINNISH*/         null,
                /*[16] DANISH*/          null,
                /*[17] PORTUGUESE*/      null,
                /*[18] CROATIAN*/        null,
                /*[19] BULGARIAN*/       null,
                /*[20] ROMANIAN*/        null,
                /*[21] LATIN*/           null,
                /*[22] ESPERANTO*/       null,
                /*[23] BOSNIAN*/         null,
                /*[24] TURKISH*/         null,
                /*[25] SERBIAN*/         null,
                /*[26] GREEK*/           null,
                /*[27] CHINESE*/         null,
                /*[28] JAPANESE*/        null,
                /*[29] SLOVENE*/         null,
                /*[30] LITHUANIAN*/      null,
                /*[31] LATVIAN*/         null,
                /*[32] ESTONIAN*/        null,
                /*[33] MALTESE*/         null
    };

    public Heinzelnisse(String label, String id, int icon) {
        super(label, id, icon);

        Boolean[][][] combinations = new Boolean[3][][];
        combinations[0] = buildBiDirMat();
        combinations[1] = combinations[0];
        combinations[2] = combinations[0];
        setCombinations(combinations);
    }

    @Override
    public void preProcess(Context c, TranslationData data) {
        PageRequest request = data.getRequest();
        String s = data.getCleanedText();
        Integer[] actLangs = data.getActiveLangs();
        setPageRequestDefaults(request);
        request.setUri(parseUri(mBaseUrls, s, mLangSpecs, actLangs));
        data.setBrowserUrl(parseUri(mBrowserUrls, s, mLangSpecs, actLangs));
    }

    @Override
    public void postProcess(Context c, TranslationData data) {
        int leadingMargin = c.getResources().getDimensionPixelOffset(R.dimen.sp_32);
        List<ParcelableCharSequence> tList = data.getTranslationList();
        tList.clear();
        PageResponse response = data.getResponse();

        try {
            JSONObject root = new JSONObject(response.get());
            JSONArray no2De = root.optJSONArray("noTrans");
            JSONArray de2Nos = root.optJSONArray("deTrans");
            if (no2De != null && no2De.length() != 0) {
                addResultsToList(tList, no2De, leadingMargin);
            }
            if (de2Nos != null && de2Nos.length() != 0) {
                addResultsToList(tList, de2Nos, leadingMargin);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (tList.size() == 0) {
            response.setError(TranslationTask.ERR_PARSING_RESPONSE);
        }
        response.set("");
    }

    private void addResultsToList(List<ParcelableCharSequence> tList, JSONArray results, int leadingMargin) {
        for (int i = 0; i < results.length(); i++) {
            MySpannableStringBuilder builder = new MySpannableStringBuilder();
            try {
                JSONObject curResult = results.getJSONObject(i);

                String wordL = curResult.getString("word");
                String wordR = curResult.optString("t_word");
                String articleL = curResult.optString("article");
                String articleR = curResult.optString("t_article");
                String otherL = curResult.optString("other");
                String otherR = curResult.optString("t_other");

                builder.append("∙ ");
                addItemsToBuilder(builder, wordL, articleL, otherL);
                builder.append("\n");
                int start = builder.length();
                addItemsToBuilder(builder, wordR, articleR, otherR);
                builder.setSpan(new LeadingMarginSpan.Standard(leadingMargin), start, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            tList.add(new ParcelableCharSequence(builder));
        }
        tList.add(new ParcelableCharSequence("\n"));
    }

    private void addItemsToBuilder(MySpannableStringBuilder builder, String word, String article, String other) {
        builder.append(word);
        int start = builder.length();
        if (article.length() != 0) {
            builder.append(" (").append(article).append(")");
        }
        if (other.length() != 0) {
            builder.append(" - ").append(other);
        }
        builder.setSpan(new StyleSpan(Typeface.ITALIC), start, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }


    private Boolean[][] buildBiDirMat() {
        Boolean[][] mat = new Boolean[mLangSpecs.length][mLangSpecs.length];
        ArrayUtils.fill(mat, false);

        Boolean[] langSpecsB = ArrayUtils.toBool(mLangSpecs);
        ArrayUtils.setFromArray(mat, langSpecsB, 0, 5, 0);
        ArrayUtils.disjunctSymmetric(mat);
        ArrayUtils.setMainDiagonal(mat, false);
        return mat;
    }
}
